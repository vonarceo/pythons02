year = int(input("Please input a year: \n"))

if ((year % 4 == 0 and year % 100 != 0) or year % 400 == 0):
    print(f"{year} is a leap year!")
else:
    print(f"{year} is not a leap year")


col = int(input("Enter number of column: \n"))
row = int(input("Enter number of row: \n"))

while col > 0:
    col -= 1
    print("*" * row)